﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.Interfaces
{
    public interface ICacheConfig
    {
        Dictionary<Type, int> cardExpirationDuration { get; set; }
    }
}
