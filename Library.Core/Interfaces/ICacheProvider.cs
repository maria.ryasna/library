﻿using Library.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.Interfaces
{
    public interface ICacheProvider
    {
        void SaveCardCache(string file, Card card);
        Card TryGetCardCache(string file); 
    }
}
