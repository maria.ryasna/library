﻿using Library.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Core.Interfaces
{
    public interface ICardStorage
    {
        List<Card> GetCardsByNumber(int number);
        void SaveCard(Card card);
    }
}