﻿using Library.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.Interfaces
{
    public interface ICardService
    {
        List<Card> GetCardsByNumber(int number);
        void SaveCard(Card card);
    }
}
