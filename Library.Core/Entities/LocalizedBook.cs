﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.Entities
{
    [Serializable]
    public class LocalizedBook : Book
    {
        public string CountryOfLocalization { get; set; }
        public string LocalPublisher { get; set; }
        public LocalizedBook() : base() { }
        public LocalizedBook(string title, DateTime datePublished, int isbn, List<Author> authors, int numberOfPages, string publisher, string countryOfLocalization, string localPublisher) 
            : base(title, datePublished, isbn, authors, numberOfPages, publisher)
        {
            CountryOfLocalization = countryOfLocalization;
            LocalPublisher = localPublisher;
        }
        public override string ToString()
        {
            return base.ToString() + $", Country of localization: {CountryOfLocalization}, Local publisher: {LocalPublisher}";
        }
    }
}
