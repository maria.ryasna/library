﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.Entities
{
    [Serializable]
    public class Book : Document
    {
        public int ISBN { get; set; }
        public List<Author> Authors { get; set; }
        public int NumberOfPages { get; set; }
        public string Publisher { get; set; }
        public Book() : base() { }
        public Book(string title, DateTime datePublished, int isbn, List<Author> authors, int numberOfPages, string publisher) : base(title, datePublished)
        {
            ISBN = isbn;
            Authors = authors;
            Publisher = publisher;
            NumberOfPages = numberOfPages;
            Publisher = publisher;
        }
        public override bool Equals(object obj)
        {
            return (obj is Book document) && ISBN == document.ISBN;
        }
        public override int GetHashCode() => ISBN.GetHashCode();
        public override string ToString()
        {
            var authString = string.Empty;
            Authors.ForEach(author => authString+=author.ToString());
            return base.ToString() + $"\n Authors: \n {authString} \n ISBN: {ISBN}, Number of pages: {NumberOfPages}, Publisher: {Publisher}";
        }
    }
}
