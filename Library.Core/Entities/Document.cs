﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.Entities
{
    [Serializable]
    public abstract class Document
    {
        public string Title { get; set; }
        public DateTime DatePublished { get; set; }
        public Document() { }
        public Document(string title, DateTime datePublished)
        {
            Title = title;
            DatePublished = datePublished;
        }
        public override string ToString()
        {
            return $"[{GetType().Name}]: Title: {Title}, Date published: {DatePublished}";
        }
    }
}
