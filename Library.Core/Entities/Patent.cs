﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.Entities
{
    [Serializable]
    public class Patent : Document
    {
        public int UniqueId { get; set; }
        public DateTime ExpirationDate { get; set; }
        public Patent() : base() { }
        public Patent(int uniqueId, DateTime expirationDate, string title, DateTime datePublished) : base(title, datePublished)
        {
            UniqueId = uniqueId;
            ExpirationDate = expirationDate;
        }
        public override bool Equals(object obj)
        {
            return (obj is Patent document) && UniqueId == document.UniqueId;
        }
        public override int GetHashCode() => UniqueId.GetHashCode();
        public override string ToString()
        {
            return base.ToString() + $", Unique id: {UniqueId}, Expiration date: {ExpirationDate}";
        }
    }
}
