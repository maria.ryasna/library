﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.Entities
{
    public class CardCache
    {
        public Card Card { get; set; }
        public DateTime? Expiration { get; set; }
        public CardCache(Card card, DateTime? expiration)
        {
            Card = card;
            Expiration = expiration;
        }
    }
}
