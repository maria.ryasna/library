﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Library.Core.Entities
{
    [Serializable]
    public class Card
    {
        public int Number { get; set; }
        public Type Type => Document.GetType();
        public Document Document { get; set; }
        public Card() { }
        public Card(int number, Document document)
        {
            Number = number;
            Document = document;
        }
        public override string ToString()
        {
            return $"Number: {Number} \n Type: {Type.Name} \n Document: \n {Document.ToString()}";
        }
    }
}
