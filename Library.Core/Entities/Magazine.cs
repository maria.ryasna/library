﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.Entities
{
    [Serializable]
    public class Magazine : Document
    {
        public string Publisher {get; set;}
        public int ReleaseNumber { get; set; }
        public Magazine() : base() { }
        public Magazine(string publisher, int releaseNumber, string title, DateTime datePublished) : base(title, datePublished)
        {
            Publisher = publisher;
            ReleaseNumber = releaseNumber;
        }
        public override bool Equals(object obj)
        {
            return (obj is Magazine document) && ReleaseNumber == document.ReleaseNumber;
        }
        public override int GetHashCode() => ReleaseNumber.GetHashCode();
        public override string ToString()
        {
            return base.ToString() + $", Publisher: {Publisher}, Release number: {ReleaseNumber}";
        }
    }
}
