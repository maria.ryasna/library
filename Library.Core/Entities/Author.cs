﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.Entities
{
    [Serializable]
    public class Author
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public Author() { }
        public Author(string name, string country)
        {
            Name = name;
            Country = country;
        }
        public override string ToString()
        {
            return $"Name: {Name}, Country: {Country}";
        }
    }
}
