﻿using Library.Core.Entities;
using Library.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Core.Services
{
    public class CacheProvider : ICacheProvider
    {
        private ICacheConfig _config { get; set; }
        private Dictionary<string, CardCache> _cache = new Dictionary<string, CardCache>();

        public CacheProvider(ICacheConfig config)
        {
            _config = config;
        }

        public void SaveCardCache(string file, Card card)
        {
            if (_config.cardExpirationDuration.ContainsKey(card.Type))
            {
                DateTime expiration = DateTime.Now.AddSeconds(_config.cardExpirationDuration[card.Type]);
                var cardCache = new CardCache(card, expiration);
                if (_cache.ContainsKey(file))
                {
                    _cache[file] = cardCache;
                }
                else
                {
                    _cache.Add(file, cardCache);
                }
            }
            else
            {
                var cardCache = new CardCache(card, null);
                if (_cache.ContainsKey(file))
                {
                    _cache[file] = cardCache;
                }
                else
                {
                    _cache.Add(file, cardCache);
                }
            }
        }

        public Card TryGetCardCache(string file)
        {
            if (_cache.ContainsKey(file))
            {
                if (_cache[file].Expiration < DateTime.Now)
                {
                    _cache.Remove(file);
                    return null;
                }
                return _cache[file].Card;
            }
            else
            {
                return null;
            }
        }
    }
}
