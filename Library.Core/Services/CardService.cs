﻿using Library.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Library.Core.Interfaces;
using Microsoft.Extensions.Caching.Memory;

namespace Library.Core.Services
{
    public class CardService : ICardService
    {
        private ICardStorage _cardStorage { get; set; }

        public CardService(ICardStorage cardStorage)
        {
            _cardStorage = cardStorage ?? throw new ArgumentNullException(nameof(cardStorage));
        }

        public List<Card> GetCardsByNumber(int number)
        {
            List<Card> cards = _cardStorage.GetCardsByNumber(number);

            return cards;
        }

        public void SaveCard(Card card)
        {
            _cardStorage.SaveCard(card);
        }
    }
}
