﻿using System.Collections.Generic;
using System.IO;
using Library.Core.Entities;
using Library.Core.Interfaces;
using System.Linq;

namespace Library.File.Storage
{
    public class FileCardStorage : ICardStorage
    {
        private string _path { get; set; }
        private ICacheProvider _cardCache { get; set; }
        public FileCardStorage(string path, ICacheProvider cardCache)
        {
            _path = path;
            _cardCache = cardCache;
        }
        public List<Card> GetCardsByNumber(int number)
        {
            var files = Directory.GetFiles(_path);

            var filteredFiles = files.ToList().FindAll(file => file.Contains(number.ToString()));

            var cards = new List<Card>();
            filteredFiles.ForEach(filePath =>
            {
                var fileName = Path.GetFileName(filePath);
                var cacheFile = _cardCache.TryGetCardCache(fileName);
                if (cacheFile != null)
                {
                    cards.Add(cacheFile);
                }
                else
                {
                    var jsonString = System.IO.File.ReadAllText(filePath);
                    var card = JsonSerializeService.DeserializeCard(jsonString);
                    cards.Add(card);
                    _cardCache.SaveCardCache(fileName, card);
                }
            });

            return cards;
        }

        public void SaveCard(Card card)
        {
            var jsonString = JsonSerializeService.SerializeCard(card);

            System.IO.File.WriteAllText($"{_path}\\{card.Type.Name}_#{card.Number}.json", jsonString);
        }
    }
}
