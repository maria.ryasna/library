﻿using Library.Core.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Library.File.Storage
{
    public class DocumentJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(Card));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jo = JObject.Load(reader);
            var docType = jo["Type"].ToObject<Type>();
            var cardNumber = jo["Number"].ToObject<int>();
            var card = new Card();
            card.Number = cardNumber;

            if (docType == typeof(LocalizedBook))
            {
                card.Document = jo["Document"].ToObject<LocalizedBook>(serializer);
            } 
            else if (docType == typeof(Book))
            {
                card.Document = jo["Document"].ToObject<Book>(serializer);
            }
            else if (docType == typeof(Patent))
            {
                card.Document = jo["Document"].ToObject<Patent>(serializer);
            }
            else if (docType == typeof(Magazine))
            {
                card.Document = jo["Document"].ToObject<Magazine>(serializer);
            }
            
            return card;
        }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {}
    }
}
