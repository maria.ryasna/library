﻿using Library.Core.Entities;
using Newtonsoft.Json;

namespace Library.File.Storage
{
    public static class JsonSerializeService
    {
        public static string SerializeCard(Card entity)
        {
            var jsonString = JsonConvert.SerializeObject(entity, Formatting.Indented);
            return jsonString;
        }

        public static Card DeserializeCard(string jsonString)
        {
            var card = JsonConvert.DeserializeObject<Card>(jsonString, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                Converters = { new DocumentJsonConverter() }
            });

            return card;
        }
    }
}
