﻿using Library.Core.Entities;
using Library.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Console.UI
{
    public class CacheConfig : ICacheConfig
    {
        public Dictionary<Type, int> cardExpirationDuration { get; set; } = new Dictionary<Type, int>();
        public CacheConfig()
        {
            cardExpirationDuration.Add(typeof(Book), 120);
            cardExpirationDuration.Add(typeof(LocalizedBook), 30);
            cardExpirationDuration.Add(typeof(Patent), 240);
        }
    }
}
