﻿using Library.Core.Entities;
using Library.Core.Interfaces;
using Library.Core.Services;
using Library.File.Storage;
using System;
using System.Collections.Generic;

namespace Library.Console.UI
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //DI
            var path = "..\\..\\..\\..\\FileStorage";
            ICacheConfig config = new CacheConfig();
            ICacheProvider cacheProvider = new CacheProvider(config);
            ICardStorage storage = new FileCardStorage(path, cacheProvider);
            ICardService service = new CardService(storage);
            CardController controller = new CardController(service);
            //DI

            bool isContinue = true;
            while (isContinue)
            {
                controller.ShowCardsByNumber();
                System.Console.WriteLine("To exist press -");
                string check = System.Console.ReadLine();
                if (check == "-")
                {
                    isContinue = false;
                }
            }
        }
    }
}
