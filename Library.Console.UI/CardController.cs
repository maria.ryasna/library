﻿using Library.Core.Entities;
using Library.Core.Interfaces;
using System;
using System.Collections.Generic;

namespace Library.Console.UI
{
    public class CardController
    {
        private ICardService _cardService { get; set; }
        public CardController(ICardService cardService)
        {
            _cardService = cardService;
        }

        public void ShowCardsByNumber()
        {
            System.Console.WriteLine("Enter card number:");
            string cardNumber = System.Console.ReadLine();

            try
            {
                List<Card> cards = _cardService.GetCardsByNumber(int.Parse(cardNumber));
                cards.ForEach(card => System.Console.WriteLine(card.ToString()));
                if (cards.Count == 0)
                {
                    System.Console.WriteLine("Not found");
                }
            }
            catch (Exception)
            {
                System.Console.WriteLine("[ERROR] Internal Server Error");
            }
            
        }
    }
}
